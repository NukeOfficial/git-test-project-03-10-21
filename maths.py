class Math():
    def division(x, y):
        if y == 0:
            raise ValueError('You can not divide by zero')
        return x/y

    def addition(x, y):
        return x+y

    def subtraction(x, y):
        return x-y

    def multiplication(x, y):
        return x*y


def collect_input():
    print("Choose Operation to perform. If you input another number, addition will be chosen.")
    print("1 -> addition")
    print("2 -> subtraction")
    print("3 -> multiplication")
    print("4 -> division")

    op = input()
    x = int(input("Type your first number: "))
    y = int(input("Type your second number: "))
    return op, x, y

def perform_operation(op, x, y):
    if op == "4":
        return Math.division(x, y)
    elif op == "2":
        return Math.subtraction(x, y)
    elif op == "3":
        return Math.multiplication(x, y)
    else:
        return Math.addition(x, y)

if __name__ == '__main__':
    op, x, y = collect_input()
    print("The result of your calculation is: ", perform_operation(op, x, y))

