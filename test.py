import unittest
from maths import Math


class Tests(unittest.TestCase):
    
    def test_divide_by_zero(self):
        with self.assertRaises(ValueError):
            Math.division(3, 0)


if __name__ == '__main__':
    unittest.main()

